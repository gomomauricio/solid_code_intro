# Código del curso Principios SOLID y Clean Code
# Instructor
> [Daniel Blanco Calviño](https://www.udemy.com/course/principios-solid-y-clean-code-escribe-codigo-de-calidad/) 
 

# Udemy
* Principios SOLID y Clean Code

## Contiene

* Clean Code.
* Test Driven Development (TDD), el Desarrollo Guiado por Pruebas.
* Code Smells.
* Principios SOLID
* Aplicar los cinco principios SOLID.
 

## Descripción
Repositorio de recursos:
 https://github.com/danielblanco96/CleanCode-SOLID-Udemy     

---

## Notas
 
* Los nombres de CLASE deben ser un nombre o un conjunto de nombres, NO VERBOS 
* Los nombres de los METODOS deben ser verbos get,set,is indicando una accion  
* Las funciones deben hacer solo una cosa
* Las funciones no deben tener argumentos o no mas de 3
* Eliminar funciones que no se usen
* No usar mas de dos lenguajes en una misma clase.




~~~
* Repositorio solo en ** Gitlab  **

/************* MAVEN ***********************/
 
 *  


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 18 FEB, 2022 ]  
 
 